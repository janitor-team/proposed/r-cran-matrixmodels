This is the Debian GNU/Linux r-cran-matrixmodels package of
MatrixModels.  The MatrixModels package extends R for modelling of
sparse and dense matriced. It was written by Douglas Bates and Martin
Maechler.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'MatrixModels' to
'r-cran-matrixmodels to fit the pattern of CRAN (and non-CRAN) packages
for R.

Files: R/*
Copyright: 2010 - 2015  Douglas Bates and Martin Maechler
License: GPL-2+

Files: debian/*
Copyright: 2014  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

   Package: MatrixModels
   Version: 0.4-0
   Date: 2015-01-13
   Title: Modelling with Sparse And Dense Matrices
   Author: Douglas Bates <bates@stat.wisc.edu> and Martin Maechler <maechler@stat.math.ethz.ch>
   Maintainer: Martin Maechler <mmaechler+Matrix@gmail.com>
   Contact: Doug and Martin <Matrix-authors@R-project.org>
   Description: Modelling with sparse and dense 'Matrix' matrices, using
     modular prediction and response module classes.
   Depends: R (>= 2.14.0), utils
   Imports: stats, methods, Matrix (>= 1.0-1)
   Encoding: UTF-8
   LazyLoad: yes
   License: GPL (>= 2)
   URL: http://Matrix.R-forge.R-project.org/
   Packaged: 2015-01-13 11:52:51 UTC; maechler
   NeedsCompilation: no
   Repository: CRAN
   Date/Publication: 2015-01-14 06:22:36
